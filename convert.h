#include <iostream>

#include <string>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;
int toint(string a){
	if (a == "0") {
		return 0;
	}
	if (a == "1") {
		return 1;
	}
	if (a == "2") {
		return 2;
	}
	if (a == "3") {
		return 3;
	}
	if (a == "4") {
		return 4;
	}
	if (a == "5") {
		return 5;
	}
	if (a == "6") {
		return 6;
	}
	if (a == "7") {
		return 7;
	}
	if (a == "8") {
		return 8;
	}
	if (a == "9") {
		return 9;
	}
	return -1;
}

int toint(char a){
	if (a == '0') {
		return 0;
	}
	if (a == '1') {
		return 1;
	}
	if (a == '2') {
		return 2;
	}
	if (a == '3') {
		return 3;
	}
	if (a == '4') {
		return 4;
	}
	if (a == '5') {
		return 5;
	}
	if (a == '6') {
		return 6;
	}
	if (a == '7') {
		return 7;
	}
	if (a == '8') {
		return 8;
	}
	if (a == '9') {
		return 9;
	}
	return -1;
}

char tochar(int a){
	if (a == 0) {
		return '0';
	}
	if (a == 1) {
		return '1';
	}
	if (a == 2) {
		return '2';
	}
	if (a == 3) {
		return '3';
	}
	if (a == 4) {
		return '4';
	}
	if (a == 5) {
		return '5';
	}
	if (a == 6) {
		return '6';
	}
	if (a == 7) {
		return '7';
	}
	if (a == 8) {
		return '8';
	}
	if (a == 9) {
		return '9';
	}
}

vector<int> reverse(vector<int> number){
	vector<int> reversed(number.size());
//	copy(number.rbegin(), number.rend(), reversed.begin());
	reverse_copy(number.begin(), number.end(), reversed.begin());
	return reversed;

}

vector<int> getNumbres(unsigned long long int number){
	vector<int> resoult;
	while(number != 0){
		resoult.push_back(number%10);
		number /= 10;
	}
	return reverse(resoult);
	
}

// vector<int> getNumbres(long long int number){
// 	vector<int> resoult;
// 	while(number != 0){
// 		resoult.push_back(number%10);
// 		number /= 10;
// 	}
// 	return reverse(resoult);
	
// }
