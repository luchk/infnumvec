//
// infNum.cpp
// Created by Taras on 11/2/19.

#include "infNum.h"
#include "convert.h"
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>


/*--------------constructors-----------------*/

infNum::infNum(){
	num.push_back(0);
}

infNum::infNum(string a){
	if(a[0] == '-'){
		this->positive = 0;
		for(int i = 1; i < a.size(); i++){
			num.push_back(toint(a[i]));
		}
	}else{
		for(auto b : a){
			num.push_back(toint(b));
		}	
	}
}

infNum::infNum(char* a){
	string temp(a);
	if(temp[0] == '-'){
		this->positive = 0;
		for(int i = 1; i < temp.size(); i++){
			num.push_back(toint(temp[i]));
			cout << "lib -" << temp[i] << endl;
		}
	}else{
		for(auto b : temp){
			num.push_back(toint(b));
			cout << "lib +" << b << endl;
		}	
	}
}

infNum::infNum(int a){
	if(a < 0){
		this->positive = 0;
		a *= -1;
	}
	this->num = getNumbres(a);
}

infNum::infNum(long a){
	if(a < 0){
		this->positive = 0;
		a *= -1;
	}
	this->num = getNumbres(a);
}

infNum::infNum(long long a){
	if(a < 0){
		this->positive = 0;
		a *= -1;
	}
	this->num = getNumbres(a);
}

infNum::infNum(unsigned int a){
	this->num = getNumbres(a);
}

infNum::infNum(unsigned long a){
	this->num = getNumbres(a);
}

infNum::infNum(unsigned long long a){
	this->num = getNumbres(a);
}

infNum::infNum(infNum &a){
	this->positive = a.positive;
	this->num = a.num;
}

/*--------------------------------------------------*/
/*--------------end-of-constructors-----------------*/
/*--------------------------------------------------*/


vector<int> infNum::getNum(){
	return this->num;
}

bool infNum::getPositive(){
	return this->positive;
}

/*---------------assignment-operators---------------*/

infNum infNum::operator = (string a){
	infNum temp(a);
	this->num = temp.num;
	return *this;
}

infNum infNum::operator = (int a){
	if(a < 0){
		a *= -1;
		this->positive = 0;
	}
	infNum temp(a);
	*this = temp;
	return *this;
}

infNum infNum::operator = (long int a){
	if(a < 0){
		a *= -1;
		this->positive = 0;
	}
	infNum temp(a);
	*this = temp;
	return *this;
}

infNum infNum::operator = (long long int a){
	if(a < 0){
		a *= -1;
		this->positive = 0;
	}
	infNum temp(a);
	*this = temp;
	return *this;
}

infNum infNum::operator = (unsigned int a){
	infNum temp(a);
	*this = temp;
	return *this;
}

infNum infNum::operator = (unsigned long int a){
	infNum temp(a);
	*this = temp;
	return *this;
}

infNum infNum::operator = (unsigned long long int a){
	infNum temp(a);
	*this = temp;
	return *this;
}

infNum infNum::operator = (vector<int> a){
	this->num = a;
	return *this;
}

infNum infNum::operator = (infNum a){
	this->positive = a.positive;
	this->num = a.num;
	return *this;
}
/*---------------------------------------------------*/
/*------------end-of-assignment-operators------------*/
/*---------------------------------------------------*/

/*--------------------operations---------------------*/
// infNum operator-();
infNum infNum::operator+(const infNum& a){
	vector<int> num1 = this->num;
	vector<int> num2 = a.num;
	vector<int> num3;
	int remainder = 0;

	if(num1.size() > num2.size()){
		int diff = num1.size() - num2.size();
		num2 = reverse(num2);
		for (int i = 0;i < diff;i++) {
			num2.push_back(0);
		}
		num2 = reverse(num2);
	}
	if(num1.size() < num2.size()){
		int diff = num2.size() - num1.size();
		num1 = reverse(num1);
		for (int i = 0;i < diff ;i++) {
			num1.push_back(0);
		}
		num1 = reverse(num1);
	}

	for(int i = num1.size() - 1; i >= 0; i--){
		int temp1 = num1[i];
		int temp2 = num2[i];
		int number = temp1 + temp2 + remainder;
		if (number >= 10){
			remainder = 1;
			number -= 10;
			num3.push_back(number);
			continue;
		} 
		remainder = 0;
		num3.push_back(number);
	}
	
	if(remainder != 0)
		num3.push_back(remainder);
	// string num4(num3.rbegin(), num3.rend());
	num3 = reverse(num3);
	infNum resoult;
	resoult = num3;
	return resoult;

}

// infNum infNum::operator-(const infNum& a);
// infNum infNum::operator*(const infNum& a);
// infNum infNum::operator/(const infNum& a); // throw
// infNum infNum::operator%(const infNum& a); // throw

// /*---------------------------------------------------*/
// /*-----------------end-of-operations-----------------*/
// /*---------------------------------------------------*/


// /*--------------operational-assignments--------------*/

// infNum& infNum::operator += (infNum& a);
// infNum& infNum::operator -= (infNum& a);
// infNum& infNum::operator *= (infNum& a);
// infNum& infNum::operator /= (infNum& a); // throw
// infNum& infNum::operator %= (infNum& a); // throw

// /*---------------------------------------------------*/
// /*-----------end-of-operational-assignments----------*/
// /*---------------------------------------------------*/

// /*--------unary-increment/decrement-operators--------*/


// infNum infNum::operator++(){
// 	infNum temp = 0;
// 	*this += 1;
// 	temp = this->num;
// 	return temp;
// }
// infNum infNum::operator--(){
// 	infNum temp = 0;
// 	*this -= 1;
// 	temp = this->num;
// 	return temp;
// }
// infNum infNum::operator++(int){
// 	infNum temp = *this;
// 	*this += 1;
	
// 	return temp;
// }
// infNum infNum::operator--(int){
// 	infNum temp = *this;
// 	*this -= 1;
// 	return temp;
// }

