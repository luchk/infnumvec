 //
// infNum.h
// Created by Taras on 11/2/19.
#include <vector>
#ifndef _infNum_h_
#define _infNum_h_
using namespace std;
class infNum{
	vector<int> num;
	bool positive = 1;
	public:
		/*--------------constructors-----------------*/
		infNum();
		infNum(string a);
		infNum(char* a);
		infNum(int a);
		infNum(long a);
		infNum(long long a);
		infNum(unsigned int a);
		infNum(unsigned long a);
		infNum(unsigned long long a);
		infNum(infNum &a);

		vector<int> getNum();
		bool getPositive();
		
		
		/*---------------assignment-operators---------------*/
		infNum operator = (string a);
		infNum operator = (int a);
		infNum operator = (long int a);
		infNum operator = (long long int a);
		infNum operator = (unsigned int a);
		infNum operator = (unsigned long int a);
		infNum operator = (unsigned long long int a);
		infNum operator = (vector<int> a);
		infNum operator = (infNum a);

		 /* operations */
		// infNum operator-();
		infNum operator+(const infNum& a);
		infNum operator-(const infNum& a);
		infNum operator*(const infNum& a);
		infNum operator/(const infNum& a); // throw
		infNum operator%(const infNum& a); // throw


		/* operational assignments */
	    infNum& operator += (infNum& a);
	    infNum& operator -= (infNum& a);
	    infNum& operator *= (infNum& a);
	    infNum& operator /= (infNum& a); // throw
	    infNum& operator %= (infNum& a); // throw


		/* unary increment/decrement operators */

		infNum operator++();
		infNum operator--();
		infNum operator++(int);
		infNum operator--(int);
		
			
};

#endif